+++
title = "Tiers lieu"
weight = 4
description = "This is the short title"
+++

Un lieu de convivialité permettant de partager du temps ou des connaissances au
travers d'ateliers, d'expérimenter, de pratiquer, de s'informer... Un lieu
polyvalent ouvert au plus grand nombre, permettant d’accueillir les évènements
d'associations ou de particuliers.


C'est sans doute le projet le plus ambitieux et le moins circonscrit à ce jour, mais concrètement nous imaginons :

- un lieu de convivialité, où une asso peut faire une
    réunion/repas/AG/3ème mi-temps, installé dans des canapés

- un lieu où l'on apprend à construire un semoir en bois, son potager vertical
- un lieu où l'on peut expérimenter des solutions low tech
- un lieu où l'on peut trouver des gens avec des compétences et l'envie de les
partager, auprès de qui on peut confronter ses idées de projets, son
imagination...
- un lieu où quelqu'un peut présenter des tuto coiffures / cuisine / informatique...
- un lieu où on arrive avec un aspirateur cassé et on repart avec un aspirateur fonctionnel


À ce sujet, nous continuons à apprendre sur les différentes organisations possibles, nous visitons d'autres tiers
lieux inspirant, nous sommes à l'écoute des éventuels participants, des présentateurs/acteurs volontaires.
