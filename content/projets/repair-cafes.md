+++
title = "Les Repair Cafés"
weight = 1
description = "Repair Café @ Thorigné"
+++
<div style="margin-bottom:2em">
<i>Nous organisons régulièrement des rencontres où nos bénévoles et partenaires partagent leurs connaissances, 
leurs compétences en bricolage, couture, astuce d'entretien...</i>
</div>

Que faire d'un grille-pain qui ne marche plus ? D'un vélo dont la roue frotte ? Ou d'un pull mité ? Les jeter ? Pas question ! La CoPa organise les Repair Cafés de Thorigné Fouillard.


<div style="display: flex; justify-content: center; margin-top: 2em; margin-bottom: 2em">
<img width="500px" src="/atelierCommun.jpg" alt="photo de trois personnes penchées au dessus d'un appareil démonté"/>
</div>

<div style="display: flex; justify-content: center; margin-top: 2em; margin-bottom: 2em">
<div class="lgw-50">

| Prochaines Dates |           Lieu           |
| :--------------- | :----------------------- |
| 19 octobre 2024  | Espace René Cassin       |
| 14 décembre 2024 | Espace René Cassin       |
| 8 février 2025   | Espace René Cassin       |
| 5 avril 2025     | Médiathèque Alfred Jarry |
| 14 juin 2025     | Espace René Cassin       |
__Règle générale__ : 2ième samedi des mois pairs.

</div>
</div>
A l'Espace René Cassin, à l’école Sainte-Anne ou la Médiathèque, nous transformons les lieux en atelier de réparation. De 14h15 à 18h, plusieurs réparateurs bénévoles sont là pour vous aider à réparer tout ce que vous avez à réparer. Outils et matériel sont également disponibles sur place. On apporte au Repair Café les choses en mauvais état: grille-pain, lampes, sèche-cheveux, vêtements, vélos, jouets, vaisselle... tout ce qui ne marche plus est bienvenu, et aura peut-être la chance d'une seconde vie. Les experts du Repair Café ont presque toujours la main heureuse.

En préconisant la réparation, la CoPa veut contribuer à réduire les montagnes de déchets.

> Nous jetons énormément, et entre autres ce qui est à peine abîmé et qui serait parfaitement réutilisable après une simple réparation. Mais pour nombre d'entre-nous, réparer n'est plus chose normale. Au Repair Café, nous voulons changer les choses.


Le Repair Café veut aussi être un lieu de rencontre où les habitants apprennent à se connaître autrement, et où ils découvrent que connaissances pratiques et expertise ne manquent pas près de chez eux.

> Réparer un vélo, un lecteur de CD ou un pantalon avec l'aide d'un voisin ou d'une voisine qu'on ne connaissait pas jusqu'alors change notre regard sur cette personne. Faire des réparations ensemble peut déboucher sur des contacts vraiment sympathiques dans le quartier.

> Réparer ne permet pas seulement d'économiser de l'argent, mais aussi de précieuses matières premières, et contribue ainsi à réduire les émissions de gaz à effet de serre. « Le Repair Café veut surtout être une expérience ludique et gratifiante de la réparation, qui s'avère souvent très simple. »

## Les Stands
### Le Stand Vélo
<div style="display: flex; justify-content: center; margin-top: 2em; margin-bottom: 2em">
<img  style="width:500px;" src="/cycle.jpeg" alt="photo de trois personnes penchées au dessus d'un appareil démonté"/>
</div>
Au stand vélo, on apprend comment entretenir son vélo et faire les réglages indispensables au bon fonctionnement et à la sécurité du cycliste. Nos passionnés de cycles vous aideront à maintenir votre véhicule favori en état de marche !

### Le Stand Couture

<div style="display: flex; justify-content: center; margin-top: 2em; margin-bottom: 2em">
<img src="/IMG_3789.JPEG" alt="photo de trois personnes penchées au dessus d'un appareil démonté"/>
</div>
Ici, on partage sur des sujets très variés, de l’accro, à l’ourlet, du sac à doc à la chaussette, de la fermeture éclair aux boutons… Apprenez à recoudre, utiliser une machine ou renforcer une couture, vous pourrez donner une longue vie ou même plusieurs à vos vêtements et accessoires !

### Le Stand Petit électroménagers / appareil électrique

<div style="display: flex; justify-content: center; margin-top: 2em; margin-bottom: 2em">
<img src="/generations.png" alt="photo de trois personnes penchées au dessus d'un appareil démonté"/>
</div>

Perceuse, grille-pains, mixeurs, aspirateurs… Les objets électriques sont partout dans notre quotidien mais peu d’entre nous ont eu l’occasion de découvrir ce qu’ils renferment. Un petit nettoyage ou une pièce défectueuse ? Vous serez guidés dans le démontage et le diagnostic.

### Le Stand Informatique
<div style="display: flex; justify-content: center; margin-top: 2em; margin-bottom: 2em">
<img src="/IMG_3790.JPEG" alt="photo de trois personnes penchées au dessus d'un appareil démonté"/>
</div>
Portables, tablettes ou encore ordinateurs  ne sont pas vraiment pensés pour être durables. Augmenter leur durée de vie est un enjeu majeur car leur fabrication est très gourmande en ressources.
Vous pourrez identifier avec l’aide de nos bénévoles les problèmes qu’ils soient matériel (disque dur à changer, mémoire à augmenter … ) ou logiciel (nettoyage, mise à jour …).


## La Fondation Repair Café Internationale

<div style="display: flex; justify-content: center">
<img width="200px" src="/RC_white.png" alt="Logo de la fondation Repair Café"/>
</div>

Le concept du [Repair Café](https://www.repaircafe.org/fr/) a vu le jour à Amsterdam, en 2009, et est l'initiative de Martine Postma, à l'époque journaliste/publiciste. En 2010, elle a créé la Fondation [Repair Café](https://www.repaircafe.org/fr/) Internationale.
Cette fondation soutient des groupes locaux dans le monde entier qui veulent démarrer leur propre Repair Café. La fondation supporte aussi le <a href="https://www.repaircafe.org/fr/cafe/repair-cafe-de-la-copa-thorigne-fouillard/">Repair Café à Thorigné Fouillard</a>.
