+++
title = "L'Objethèque, ou la mutualisation de nos objets"
weight = 2
description = "Objethèque @ Thorigné"
+++

<p>
Dans nos foyers, on retrouve bon nombre d'équipements que nous achetons mais que nous n'utilisons finalement que très peu.
C'est tout à fait le genre d'achats qui pourraient facilement être évité, si une solution alternative existait.
</p>
<div style="position:relative;display: flex; flex-direction:column;align-items: center; margin-top: 2em; margin-bottom: 2em">
<img width="500px" src="/ObjethequeCornouaille_mdestjan-960x640.jpg" alt="photo d'une étagère sur lequel sont rangés des outils, et du petit éléctroménager"/>
 <div style="font-size: xx-small;" >
                    <div>Crédit Côté Quimper</div>
 </div>
</div>


<p>
A l'image d'une bibliothèque, L'objethèque proposent d'organiser le stockage et la mise à disposition d'équipements partagés.
Voici quelques exemples d'équipements : tondeuse, porte vélo, remorque, grand véhicule, appareils de cuisine (raclette, fondu...), sono, broyeur végétaux, diable, barnum... 
</p>