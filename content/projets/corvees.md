+++
title = "Les corvées partagées"
weight = 3
description = "Les corvées partagées"
+++

<div >
<div>
<p>
Dans notre quotidien moderne, nous avons l'injonction de devoir d'être autonome, de ne pas dépendre des autres. C'est un marqueur de succès.
Nos villes ne sont plus des villages où la vie s'est organisée comme un clan solidaire, mais plutôt par des individus 
qui ont simplement trouvé un logement convenable, une localisation compatible avec leurs habitudes de travail, leurs contraintes familiales...
</p><p>
Il n'est pas question ici de vouloir revenir à l'époque où tout un village se retrouvait pour les moissons,
sous forme d'une corvée partagée puis d'une grande fête de village. Mais, ce vieux modèle rural avait pourtant quelques valeurs intéressantes que nous pouvons aspirer à retrouver.  
</p>
<p>
Aussi, la Communauté de Partage souhaite impulser un retour aux corvées partagées (et biensur, en bonus, aux moments de convivialité qui les suivent !).
Bien plus qu'une prestation de service, plus d'une échange de service, la Corvée Partagée permet de regrouper
sur un même lieu, un même jour et pour un même objectif, plusieurs volontaires pour, simplement, faire à plusieurs. Ce partage, ce Faire-Ensemble permet à chacun 
de faire plus vite, de déjouer la lassitude, l'ennui ou la fatigue de la tâche et donne l'occasion de partager au final la satisfaction du travail terminé.
</p>
<div style="position:relative;display: flex; flex-direction:column;align-items: center; margin-top: 2em; margin-bottom: 2em">
<img width="500px" src="/firewood-hands.jpg" alt="photo de deux personnes portant des buches"/>
 <div style="font-size: xx-small;" >
                    <div>Credit by prostooleh on Freepik</div>
 </div>
</div>


<blockquote>
<p>En Général je rentre, seul, 6 stères de bois par an, et ca me prend tout un week end... Et je fini épuisé et courbaturé !
En partageant cette tâche avec mon voisin, nous avons rentré nos deux tas sur la même journée, c'était bien plus efficace. 
On a commencé par mon jardin le matin, puis le sien  l'après midi. Le soir, on a fini par improviser un repas partagé. 
J'ai quand même eu des courbatures, mais la corvée passe bien mieux à plusieurs !</p> 
</blockquote></div>

<div style="margin-top:2em">
<p>
Si vous chercher un binome, si vous souhaitez organiser une corvée partagée, n'hésitez pas à <a href="mailto:info@communautesdepartage.fr?subject=Nouvelle%20Corvée%20Pargagée">nous contacter !</a> <br/>
Tous les prétextes sont bons : batch cooking, taille de haies, conserves, patisserie...  
</p>
</div>
</div>