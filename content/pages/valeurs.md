+++
title = "Qui sommes nous ?"
weight = 1
+++

# Qui sommes nous ?

Nous sommes une association 1901 implantée à Thorigné Fouillard. Nous proposons des services aux particuliers pour
réduire leur empreinte écologique. Nous nous attaquons à nos habitudes de consommation pour tendre vers la
décroissance.

<div style="display: flex; justify-content: center; margin-top: 2em; margin-bottom: 2em">
<img src="/IMG_20240127_135739.jpg" alt="photo de nos bénévoles souriant devant l'école sainte anne, entourant un panneau Repair Café"/>
</div>

## Nos Ambitions

- Enrayer notre société de consommation et en particulier le réflexe : J'ai besoin, j’achète.
- Proposer de faire un pas, facilement acceptable par le plus grand nombre, vers une consommation plus
  durable (à l'image des mouvements de la décroissance, de la simplicité volontaire, ou encore de la
  sobriété)
- Mettre en œuvre une approche "virale" ou "venant du bas" plus propice à une révolution des usages
- Stimuler le partage, la cohésion et la solidarité à une époque où les conditions climatiques nous
  imposeront ces "compétences"

## Nos Valeurs

### L'écologie pragmatique

- Il n'y a pas de course au plus écologique. Il n'existe pas d'écolo parfait. Chacun doit pouvoir
  définir son propre niveau d'écologie sur les différents domaines de sa vie, par exemple, être libre
  d'utiliser une solution thermique sans être montré du doigt.
- Même si l'équipement n'est pas une solution parfaite du point de vu écologique, elle peut avoir sa
  place et ses usages. Ex: Il vaut mieux réutiliser du matériel d'occasion que de le jeter pour une
  version bio-sourcée.
- Il vaut mieux partager une ressource, même "polluante", à plusieurs, que de laisser chacun acheter
  la sienne dans son coin.
- Proposer des alternatives plus écologiques quand c'est possible, sensibiliser aux changements
  d'usages (ex: tondeuse manuelle, ou ne plus tondre (ex gazon sauvage / japonais) )
- Les achats doivent faire l'objet d'une étude sur la réparabilité et l'empreinte écologique
  (construction + usage + entretien + recyclage)
- Réparer c'est mieux que remplacer

### La Solidatité et la Cohésion

La caractère associatif des communautés de partage est important : Ce n'est pas un service municipal. Les adhérents
se regroupent autour d'un projet commun qui leur impose de collaborer, résoudre des conflits, définir un mode de
gouvernance, prendre des décisions à plusieurs... C'est la base du vivre ensemble et pourtant dans la vie du
citoyen, nous avons peu d'opportunité de rentrer en relation avec les gens qui nous entourent.
