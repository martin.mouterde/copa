+++
title = "Contacts"
weight = 3
+++

# Contacter la CoPa

[info@communautesdepartage.fr](mailto://info@communautesdepartage.fr)


## Liste de diffusion

[S'inscrire ici](https://framalistes.org/sympa/subscribe/copa-tf) pour recevoir notre actualité.

## Salon de discussion en ligne

[S'inscrire ici/se connecter ici](https://framateam.org/signup_user_complete/?id=386dy67iwjnd3nhfdpb7bbfh7a&md=link&sbr=su) à notre salon Mattermost

## Nous rejoindre

L'adhésion à l'association est annuelle, de date à date, le montant est de 10€.

Deux solutions : 
- [Inscription en ligne](https://la-communaute-de-partage.s2.yapla.com/fr/adh-sion-2024-2025-15202/adhesion/)
- [Inscription papier](/bulletinAdhesion.pdf)

Modes de paiement : 
  - Carte Bancaire (inscription en ligne seulement)
  - Espèces
  - Chèque à l'ordre de "Copa"
  - Virement (FR76 1360 6000 9246 3467 4918 010)
