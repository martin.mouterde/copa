+++
title = "Agenda"
weight = 2
+++

# L'agenda de la CoPa

Retrouver les prochains événements de la COPA et de projets  proches à la fois géograhiquement et thématiquement: repair café, ateliers, partages ...

<iframe width="100%" height="800" style="margin-top:2em" src="https://copa.frama.space/apps/calendar/embed/gkfA2NzwF3Zq6WZ3"></iframe>
