+++
title = "Mentions Légales"
weight = 3
+++

# Mentions Légales

## Raison sociale
<p>Communauté de Partage<br/>
5 rue montaigne<br/>
35235 Thorigné Fouillard<br/>
 </p>
<p>Directeur de publication : Martin Mouterde - info@communautesdepartage.fr</p>

## Hébergement
<p>GitLab Pages from framagit.org<br/>
Hetzner Online GmbH<br/>
Industriestr. 25<br/>
91710 Gunzenhausen<br/>
Germany<br/>
Tél.: +49 (0)9831 505-0 </p>